"""blogo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

from blog import views

urlpatterns = [
    url(r'^$', views.index, name='view_index'),
	url(r'^post/(?P<shorttitle>[^\.]+).html', views.view_post, name='view_blog_post'),
	url(r'^category/(?P<shorttitle>[^\.]+).html', views.view_category, name='view_category'),
	url(r'^tag/(?P<shorttitle>[^\.]+).html', views.view_tag, name='view_tag'),
	url(r'^author/(?P<author>[^\.]+).html', views.view_author, name='view_author'),
	url(r'^login.html', views.login, name='login'),
	url(r'^register.html', views.register, name='register'),
    url(r'^admin/', admin.site.urls),
]