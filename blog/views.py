from django.shortcuts import render_to_response, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from blog.models import BlogPost, BlogCategory, BlogTag

def view_post(request, shorttitle):
	post = get_object_or_404(BlogPost, shorttitle=shorttitle)
	return render_to_response('view_post.html', {'tags': view_side_tags(), 'categories':view_side_categories(), 'post': post})

def index(request):
	return render_to_response('view_paginator.html', {'tags': view_side_tags(), 'categories': view_side_categories(), 'posts': view_paginator(BlogPost.objects.all(), request)})

def view_category(request, shorttitle):
	category = get_object_or_404(BlogCategory, shorttitle=shorttitle)
	posts = BlogPost.objects.filter(category=category)
	return render_to_response('view_paginator.html', {'tags': view_side_tags(), 'categories':view_side_categories(), 'posts': view_paginator(posts, request)})
	
def view_tag(request, shorttitle):
	tag = get_object_or_404(BlogTag, shorttitle=shorttitle)
	posts = BlogPost.objects.filter(tags=tag)
	return render_to_response('view_paginator.html', {'tags': view_side_tags(), 'categories': view_side_categories(), 'posts': view_paginator(posts, request)})
	
def view_author(request, author):
	tag = get_object_or_404(BlogTag, shorttitle=author)
	posts = BlogPost.objects.filter(tags=tag)
	return render_to_response('view_paginator.html', {'tags': view_side_tags(), 'categories': view_side_categories(), 'posts': view_paginator(posts, request)})
	
def view_paginator(posts_all, request):
	page = request.GET.get('page', 1)
	paginator = Paginator(posts_all, 2)
	
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)
	
	return posts
	
def view_side_categories():
	return BlogCategory.objects.all()
	
def view_side_tags():
	return BlogTag.objects.all()

def login(request):
	return render_to_response('login.html')
	
def register(request):
	return render_to_response('register.html')