from django.db import models
from django.db.models import permalink
from django.utils import timezone

class BlogPost(models.Model):
	id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=255, unique=True)
	shorttitle = models.SlugField(max_length=255, unique=True)
	body = models.TextField()
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True, null=True)
	author = models.ForeignKey('auth.User')
	category = models.ForeignKey('blog.BlogCategory')
	tags = models.ManyToManyField('blog.BlogTag')

	def __unicode__(self):
		return '%s' % self.title

	@permalink
	def get_absolute_url(self):
		return ('view_blog_post', None, { 'shorttitle': self.shorttitle })

class BlogCategory(models.Model):
	id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=255, db_index=True)
	shorttitle = models.SlugField(max_length=100, unique=True)

	def __unicode__(self):
		return '%s' % self.title

	@permalink
	def get_absolute_url(self):
		return ('view_category', None, { 'shorttitle': self.shorttitle })

class BlogTag(models.Model):
	id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=255, unique=True)
	shorttitle = models.SlugField(max_length=255, unique=True)

	def __unicode__(self):
		return '%s' % self.title

	@permalink
	def get_absolute_url(self):
		return ('view_tag', None, { 'shorttitle': self.shorttitle })